Madrid
======

- [Bars](#Bars)
- [Zum essen (Gastronomie)](#Gastronomie)
- [Architektur](#Architektur)


Gastronomy
----
### Bites
Spain is well known for its rich and very varied gastronomy. Each region counts on its own specialities, and Madrid also has its own traditional dishes.

### Drinks
- #### __Beer__:

 The mother of all drinks, beloved and worshipped. Differently to Germany, in Spain it usually comes in small formats (which makes sense in hot weather, who likes warm beer?). The most famous brand from Madrid is `Mahou`, but there are other brands also worth tasting. There is a recent tradition of little breweries and artesanal beer, like `La Cibeles` among many others. You normally would order a caña, but there are other options.
 - ##### Tap:
    - ___Caña:___ _Semper fidelis_:
 Small beer from tap, normally 20 or 25 cl, always young, with its topping of thick foam (otherwise you got a very bad waiter), always with her sister the tapa (what else!)
    - ___Doble:___ A bigger (or _double_) caña. Good if you are thirsty or you are German.
    - ___Jarra:___ 0.5 L. from tap. You are either exaggerating, or a tourist.
    - ___Mini:___ 1L., you would get that on rock bars, street parades and music festivals.
  - ##### Bottle:
   - ___Quinto:___ Stands for _fifth_, so 20cl bottle, 1/5 of a liter. If you order a quinto, you drink _from_ the quinto. Never ask for a glass!
   - ___Tercio:___ Stands for _third_, no much more to add. Not really attractive.

- #### __Vermut (vermú)__:
 This is one of the most typical drinks from Madrid, usually drank before foods (_aperitivo_). White wine flavoured with several herbs, served in a short glass with ice cubes and sometimes citricus. Normally home made. A __must__.  

- #### __Wine__:
 Wine is also quite good in Spain, and since the production is quite big, it is significantly cheaper than in northern Europe. Take advantage of this.
 - __Finos and Manzanilla:__ White wines, very exclusive from the south of Spain, similar to Sherry (`Jerez`) but with a unique taste. Worth trying with some olives and some Serrano before lunch ([_Venencia_](#Venencia) is a perfect place for this)
 - __Whites:__ `Ribeiro` (cold, goes perfectly with fish and seafood, easier to find in Asturian bars and restaurants), `Rías Baixas` (Galicia)
 - __Red:__ Thick, dense wines, ideal with meat. Look for the _Denominación de origen_, the quality label that normally gives each wine its character based on the geographical properties of the region where it was grown and made. Some of the best ones are `Ribera del Duero`, `Rioja`, `Valdepeñas`, `Toro` or `Rueda`.

- #### __Longdrinks__:
We are generous people and thus we honour the name. ___Long___drink: not short. In traditional places in a thick, tall glass. There are no standard measures, so be sure to smile the waiter to get a favourable proportion. `Gintonic`, `cubalibre`.

- #### __Spirits__:
 - __Orujo de hierbas__: Definitely a must. Normally home made from grapes and aromatic herbs, which give it a green colour. Sometimes the herbs are kept inside the bottle (don´t expect any labels on it!) A slow shot after a good meal, heaven.  

### Bars
- [`Bodega de la Ardosa`](https://www.tripadvisor.es/Restaurant_Review-g187514-d1464604-Reviews-Bodega_de_la_Ardosa-Madrid.html) _Colón, 13_

 Tiny bar, or at least at first glance. Crawl under the bar to find the back room, decorated with old pictures. Good tapas, try their amazing __tortilla__

- [`La Venencia`](https://www.tripadvisor.es/Attraction_Review-g187514-d607152-Reviews-La_Venencia-Madrid.html) _Echegaray, 7_

 Tin

### Restaurants
